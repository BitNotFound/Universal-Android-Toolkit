# Universal-Android-Toolkit

UAT is NOT funtional at the moment. Please check back for updates on this page.

UAT is built for Linux and OSX and serves as a one-stop-shop for all manner of Android devices for (un)locking bootloaders, backing up, flashing, etc. Some devices will have a feature for restoring factory ROMs, if they are publicly available.
